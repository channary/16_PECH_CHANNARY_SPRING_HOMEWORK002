package com.example.homework002.controller;

import com.example.homework002.model.entity.Product;
import com.example.homework002.model.request.ProductRequest;
import com.example.homework002.model.response.ProductResponse;
import com.example.homework002.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-product")
    @Operation(summary = "Get all authors")

    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct(){

        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .playload(productService.getAllProducts())
                .message("get all product successful !!")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }


    @GetMapping("/get-product-by-id/{id}")
    @Operation(summary = "Get Product by id")
    public ResponseEntity<ProductResponse<Product>>  getProductById(@PathVariable("id") Integer productId){
        ProductResponse<Product> response = null;
        if(productService.getProductById(productId) != null){
            response = ProductResponse.<Product>builder()
                    .playload(productService.getProductById(productId))
                    .message("get product successful !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            return ResponseEntity.badRequest().body(response);
        }
    }


    @DeleteMapping("/delete-product-by-id/{id}")
    public ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable("id") Integer productId){
        ProductResponse<String> response = null;
        if (productService.deleteProductById(productId) == true){
            response = ProductResponse.<String>builder()
                    .message("Delete this product is successful !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            return ResponseEntity.badRequest().body(response);
        }

    }

    @PostMapping("/add-new-product")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest){
        Integer addProductId = productService.addNewProduct(productRequest);
        System.out.println(addProductId);
        if ( addProductId != null){
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .playload(productService.getProductById(addProductId))
                    .message("Insert Product is successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            return null;
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductResponse<Product>> updateProductById(
            @RequestBody ProductRequest productRequest, @PathVariable("id") Integer productId)
    {
        Integer idProductUpdate = productService.updateProduct(productRequest,productId);
        ProductResponse<Product> response = null;
        if(idProductUpdate != null){
            response = ProductResponse.<Product>builder()
                    .playload(productService.getProductById(idProductUpdate))
                    .message("Update Product is successfully")
                    .success(true)
                    .build();
            return  ResponseEntity.ok(response);
        }
        else {
            return ResponseEntity.notFound().build();
        }

    }

}
