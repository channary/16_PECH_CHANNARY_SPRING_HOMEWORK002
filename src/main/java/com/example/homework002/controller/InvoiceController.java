package com.example.homework002.controller;

import com.example.homework002.model.entity.Invoice;
import com.example.homework002.model.request.InvoiceRequest;
import com.example.homework002.model.response.InvoiceResponse;
import com.example.homework002.service.InvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice/")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all-invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice(){
        InvoiceResponse<List<Invoice>> response = null;
        if(invoiceService.getAllInvoice() != null){
            response = InvoiceResponse.<List<Invoice>>builder()
                    .playload(invoiceService.getAllInvoice())
                    .message("Fetch data successfully")
                    .success(true)
                    .build();

        }
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById(@PathVariable("id") Integer invoiceId){
        InvoiceResponse<Invoice> response = null;
        if(invoiceService.getInvoiceById(invoiceId) != null){
            response = InvoiceResponse.<Invoice>builder()
                    .playload(invoiceService.getInvoiceById(invoiceId))
                    .message("Fetch data successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            return ResponseEntity.badRequest().body(response);
        }

    }

    @PostMapping("/add-new-invoice")
    public ResponseEntity<InvoiceResponse<Invoice>> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){
        InvoiceResponse<Invoice> response = null;

        Integer storeInvoiceId = invoiceService.addNewInvoice(invoiceRequest);

            response = InvoiceResponse.<Invoice>builder()
                    .playload(invoiceService.getInvoiceById(storeInvoiceId))
                    .message("Insert data successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);

    }

    @PutMapping("/update-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<Invoice>> updateInvoice(
            @RequestBody InvoiceRequest invoiceRequest,@PathVariable("id") Integer invoiceId)
    {
        Integer InvoiceIdUpdate = invoiceService.updateInvoice(invoiceRequest,invoiceId);
        InvoiceResponse<Invoice> response = null;
        if (InvoiceIdUpdate != null){
            response = InvoiceResponse.<Invoice>builder()
                    .playload(invoiceService.getInvoiceById(invoiceId))
                    .message("Invoice update successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            return ResponseEntity.badRequest().body(response);
        }

    }

    @DeleteMapping("/delete-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<String>> deleteInvoiceById(
            @PathVariable("id") Integer invoiceId)
    {
        InvoiceResponse<String> response = null;
        if (invoiceService.deleteInvoiceById(invoiceId) == true){
            response = InvoiceResponse.<String>builder()
                    .message("Delete Invoice Successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            return ResponseEntity.badRequest().body(response);
        }
    }

}
