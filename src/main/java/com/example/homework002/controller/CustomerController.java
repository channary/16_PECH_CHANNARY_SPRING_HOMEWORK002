package com.example.homework002.controller;

import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.request.CustomerRequest;
import com.example.homework002.model.response.CustomerResponse;
import com.example.homework002.repository.CustomerRepository;
import com.example.homework002.service.CustomerService;
import com.example.homework002.service.serviceImp.CustomerServiceImp;
import org.apache.ibatis.annotations.Delete;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")

public class CustomerController {
    private final CustomerService customerService;
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get-all-customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer(){
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .playload(customerService.getAllCustomer())
                .message("Fetch all customer is successfully !!")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer customerId){
        CustomerResponse<Customer> response = null;
        if(customerService.getCustomerById(customerId) != null){
            response = CustomerResponse.<Customer>builder()
                    .playload(customerService.getCustomerById(customerId))
                    .message("This customer was found !!")
                    .success(true)
                    .build();
            return  ResponseEntity.ok(response);
        }else {
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/add-new-customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        Integer addCustomerId = customerService.addNewCustomer(customerRequest);
        System.out.println(addCustomerId);
        if(addCustomerId != null){
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .playload(customerService.getCustomerById(addCustomerId))
                    .message("Insert customer is successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            return null;
        }
    }

    @PutMapping("/update-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomerId(
        @RequestBody CustomerRequest customerRequest, @PathVariable("id") Integer customerId)
    {
        Integer idCustomerUpdate = customerService.updateCustomer(customerRequest,customerId);
        CustomerResponse<Customer> response = null;
        if(idCustomerUpdate != null){
            response = CustomerResponse.<Customer>builder()
                    .playload(customerService.getCustomerById(idCustomerUpdate))
                    .message("Update Product is successfully")
                    .success(true)
                    .build();
            return  ResponseEntity.ok(response);
        }
        else {
            return null;
        }
    }

    @DeleteMapping("/delete-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<String>> deleteCustomerById(@PathVariable("id") Integer customerId){
        CustomerResponse<String> response = null;
        if(customerService.deleteCustomerById(customerId) == true){
            response = CustomerResponse.<String>builder()
                    .message("Remove Customer successfully !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            return ResponseEntity.badRequest().body(response);
        }

    }




}
