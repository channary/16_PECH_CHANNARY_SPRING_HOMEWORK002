package com.example.homework002.service.serviceImp;

import com.example.homework002.model.entity.Invoice;
import com.example.homework002.model.request.InvoiceRequest;
import com.example.homework002.repository.InvoiceRepository;
import com.example.homework002.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InvoiceServiceImp implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {
        Integer storeInvoiceId =  invoiceRepository.storeNewInvoice(invoiceRequest);
        for (Integer productId: invoiceRequest.getProductId()){
            invoiceRepository.saveProductByInvoiceId(storeInvoiceId,productId);
        }
        return storeInvoiceId;
    }

    @Override
    public Integer updateInvoice(InvoiceRequest invoiceRequest, Integer invoiceId) {
        Integer updateInvoiceId = invoiceRepository.updateInvoiceId(invoiceRequest,invoiceId);
        for (Integer productId: invoiceRequest.getProductId()){
            invoiceRepository.updateProductByInvoiceId(productId,invoiceId);
        }
        return updateInvoiceId;
    }

    @Override
    public Boolean deleteInvoiceById(Integer invoiceId) {
        return invoiceRepository.deleteInvoiceById(invoiceId);
    }


}
