package com.example.homework002.service.serviceImp;

import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.request.CustomerRequest;
import com.example.homework002.repository.CustomerRepository;
import com.example.homework002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImp implements CustomerService{
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        return customerRepository.insertCustomer(customerRequest);
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest,Integer customerId) {
        return customerRepository.updateCustomer(customerRequest,customerId);
    }

    @Override
    public Boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerById(customerId);
    }
}
