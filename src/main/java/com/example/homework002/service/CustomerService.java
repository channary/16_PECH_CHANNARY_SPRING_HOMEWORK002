package com.example.homework002.service;

import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.request.CustomerRequest;
import com.example.homework002.model.response.CustomerResponse;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();

    Customer getCustomerById(Integer customerId);
    Integer addNewCustomer(CustomerRequest customerRequest);

    Integer updateCustomer(CustomerRequest customerRequest,Integer customerId);

    Boolean deleteCustomerById(Integer customerId);

}
