package com.example.homework002.service;

import com.example.homework002.model.entity.Invoice;
import com.example.homework002.model.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(Integer invoiceId);

    Integer addNewInvoice(InvoiceRequest invoiceRequest);

    Integer updateInvoice(InvoiceRequest invoiceRequest,Integer invoiceId);

    Boolean deleteInvoiceById(Integer invoiceId);
}
