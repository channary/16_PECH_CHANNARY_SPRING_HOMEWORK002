package com.example.homework002.service;

import com.example.homework002.model.entity.Product;
import com.example.homework002.model.request.ProductRequest;
import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();

    Product getProductById(Integer productId);

    Boolean deleteProductById(Integer productId);

    Integer addNewProduct(ProductRequest productRequest);

    Integer updateProduct(ProductRequest productRequest, Integer productId);


}
