package com.example.homework002.repository;

import com.example.homework002.model.entity.Invoice;
import com.example.homework002.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("SELECT * FROM invoice_tb")
    @Results(id = "invoiceMap",value = {
            @Result(property = "invoiceId", column = "invoice_id"),
            @Result(property = "invoiceDate", column = "invoice_date"),
            @Result(property = "customer", column = "customer_id",
                    one = @One(select = "com.example.homework002.repository.CustomerRepository.getCustomerById")
            ),
            @Result(property = "products", column = "invoice_id",
                many = @Many(select = "com.example.homework002.repository.ProductRepository.getProductByIdInnerJoinInvoiceDetail"))

    })
    List<Invoice> findAllInvoice();


    @Select("SELECT * FROM invoice_tb WHERE invoice_id = #{invoiceId}")
    @ResultMap("invoiceMap")
    Invoice getInvoiceById(Integer invoiceId);

    @Select("INSERT INTO invoice_tb (invoice_date, customer_id) " +
            "VALUES (#{request.invoiceDate}, #{request.customerId}) " +
            "RETURNING invoice_id")
    @Result(property = "invoiceId", column = "invoice_id")
    Integer storeNewInvoice(@Param("request") InvoiceRequest invoiceRequest);

    @Insert("INSERT INTO invoice_detail_tb (invoice_id, product_id) " +
            "VALUES (#{invoiceId}, #{productId})")
    void saveProductByInvoiceId(Integer invoiceId, Integer productId);

    @Select("UPDATE invoice_tb " +
    "SET customer_id = #{request.customerId}, " +
            "invoice_date = #{request.invoiceDate} " +
    "WHERE invoice_id = #{invoiceId} " +
    "RETURNING invoice_id")
    Integer updateInvoiceId(@Param("request") InvoiceRequest invoiceRequest, Integer invoiceId);

    @Select("UPDATE invoice_detail_tb " +
            "SET invoice_id = #{invoiceId}, " +
            "product_id = #{productId} " +
            "WHERE invoice_id = #{invoiceId} ")
    Integer updateProductByInvoiceId(Integer productId, Integer invoiceId);

    @Delete("DELETE FROM invoice_tb WHERE invoice_id = #{id}")
    Boolean deleteInvoiceById(@Param("id") Integer invoiceId);


}
