package com.example.homework002.repository;

import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.request.CustomerRequest;
import com.example.homework002.model.request.ProductRequest;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("SELECT * FROM customer_tb")
       @Results(id = "customerMap",value = {
                @Result(property = "customerId", column = "customer_id"),
                @Result(property = "customerName", column = "customer_name"),
                @Result(property = "customerAddress", column = "customer_address"),
                @Result(property = "customerPhone", column = "customer_phone")
        })

    List<Customer> findAllCustomer();

    @Select("SELECT * FROM customer_tb WHERE customer_id = #{customerId}")
    @ResultMap("customerMap")
    Customer getCustomerById(Integer customerId);

    @Select("INSERT INTO customer_tb (customer_name,customer_address,customer_phone) VALUES(#{request.customerName}," +
            "#{request.customerAddress},#{request.customerPhone}) " +
            "RETURNING customer_id")
    Integer insertCustomer(@Param("request") CustomerRequest customerRequest);

    @Select("UPDATE customer_tb " +
    "SET customer_name = #{request.customerName}," +
    "customer_address = #{request.customerAddress}," +
            "customer_phone = #{request.customerPhone} " +
            "WHERE customer_id = #{customerId} " +
            "RETURNING customer_id" )
    Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer customerId);

    @Delete("DELETE FROM customer_tb where customer_id = #{customerId}")
    Boolean deleteCustomerById(@Param("customerId") Integer customerId);
}
