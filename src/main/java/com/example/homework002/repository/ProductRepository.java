package com.example.homework002.repository;


import com.example.homework002.model.entity.Product;
import com.example.homework002.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("SELECT * FROM product_tb")
    @Results(id = "productMap",value = {
            @Result(property = "productId", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price")
    })

    List<Product> findAllProduct();

    @Select("SELECT * FROM product_tb where product_id = #{productId}")
    @ResultMap("productMap")
    Product getProductById(Integer productId);

    @Delete("DELETE FROM product_tb where product_id = #{id}")
    Boolean deleteProductById(@Param("id") Integer productId);

    @Select("INSERT INTO product_tb (product_name,product_price) VALUES(#{request.productName}, #{request.productPrice}) " +
            "RETURNING product_id")
//    @ResultMap("productMap")
    Integer insertProduct(@Param("request") ProductRequest productRequest);

    @Select("UPDATE product_tb " +
            "SET product_name = #{request.productName}," +
            "product_price = #{request.productPrice} " +
            "WHERE product_id = #{productId} " +
            "RETURNING product_id" )
    Integer updateProduct(@Param("request") ProductRequest request, Integer productId);


    @Select("SELECT p.product_id, p.product_name, p.product_price FROM invoice_detail_tb ind " +
            "INNER JOIN product_tb p ON p.product_id = ind.product_id " +
            "WHERE ind.invoice_id = #{invoiceId}")
    @ResultMap("productMap")
    List<Product> getProductByIdInnerJoinInvoiceDetail(Integer invoiceId);
}
