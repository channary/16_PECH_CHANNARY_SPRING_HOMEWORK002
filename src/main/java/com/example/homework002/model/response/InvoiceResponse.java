package com.example.homework002.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoiceResponse<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T playload;
    private String message;
    private boolean success;
}
