package com.example.homework002.model.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerResponse<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T playload;
    private String message;
    private boolean success;
}
