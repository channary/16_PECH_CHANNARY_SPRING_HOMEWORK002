package com.example.homework002.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Invoice {
    private Integer invoiceId;
    private String invoiceDate;
    private Customer customer;
    private List<Product> products = new ArrayList<>();
}
