package com.example.homework002.model.request;

import com.example.homework002.model.entity.Customer;
import com.example.homework002.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoiceRequest {
    private Timestamp invoiceDate;
    private Integer customerId;
    private List<Integer> productId;
}
