package com.example.homework002.model.request;
import lombok.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class CustomerRequest{
    private String customerName;
    private String customerAddress;
    private String customerPhone;
}
