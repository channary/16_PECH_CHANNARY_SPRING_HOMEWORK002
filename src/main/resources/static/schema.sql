CREATE TABLE product_tb(
                          product_id SERIAL PRIMARY KEY ,
                          product_name VARCHAR(50) NOT NULL ,
                          product_price FLOAT(20) NOT NULL
);



CREATE TABLE customer_tb(
    customer_id SERIAL PRIMARY KEY,
    customer_name VARCHAR(255) NOT NULL,
    customer_address  VARCHAR(255) NOT NULL,
    customer_phone VARCHAR(15) NOT NULL
);

CREATE TABLE invoice_tb(
       invoice_id SERIAL PRIMARY KEY ,
       invoice_date TIMESTAMP,
       customer_id INT REFERENCES customer_tb(customer_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE invoice_detail_tb(
      id SERIAL PRIMARY KEY ,
      invoice_id INT REFERENCES invoice_tb(invoice_id) ON UPDATE CASCADE ON DELETE CASCADE ,
      product_id  INT REFERENCES product_tb(product_id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- TESTING

-- INSERT INTO invoice_tb (invoice_date, customer_id)
--             VALUES ('2-9-2023', 10)
--             RETURNING invoice_id;
--
--
-- INSERT INTO invoice_detail_tb (invoice_id, product_id)
--             VALUES (11, 10);
--
--
-- UPDATE invoice_tb SET customer_id = 7 where invoice_id = 4;
--
-- DELETE FROM invoice_detail_tb WHERE invoice_id = 4;
--
-- INSERT INTO invoice_detail_tb (invoice_id, product_id)
--             VALUES (4, 8);
--
--
-- DELETE FROM invoice_tb where invoice_id = 4;

